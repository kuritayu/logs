package logs

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
)

type LogStream struct {
	Logs
	LogGroupName string
	Input        cloudwatchlogs.DescribeLogStreamsInput
}

func (log Logs) LogStream(logGroupName string) *LogStream {
	logStream := &LogStream{
		Logs:         log,
		LogGroupName: logGroupName,
		Input:        cloudwatchlogs.DescribeLogStreamsInput{LogGroupName: aws.String(logGroupName)},
	}

	return logStream

}

func (ls LogStream) FindAll() ([]string, error) {
	var logStreams []string

	for {
		items, err := ls.Logs.client.DescribeLogStreams(&ls.Input)
		if err != nil {
			return nil, err
		}

		for _, item := range items.LogStreams {
			logStreams = append(logStreams, aws.StringValue(item.LogStreamName))
		}

		if aws.StringValue(items.NextToken) == "" {
			break
		}

		ls.Input.NextToken = items.NextToken

	}

	return logStreams, nil
}

func (ls LogStream) Save(logStream string) error {

	input := &cloudwatchlogs.CreateLogStreamInput{
		LogGroupName:  aws.String(ls.LogGroupName),
		LogStreamName: aws.String(logStream),
	}

	_, err := ls.Logs.client.CreateLogStream(input)

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case cloudwatchlogs.ErrCodeResourceAlreadyExistsException:
				return nil
			default:
				return err
			}
		}

	}

	return nil

}
