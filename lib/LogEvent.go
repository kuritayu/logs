package logs

import (
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
)

type LogEvent struct {
	Logs
	LogGroupName  string
	LogStreamName string
	Input         cloudwatchlogs.GetLogEventsInput
}

type LogContent struct {
	TimeStamp int64
	Message   string
}

func (log Logs) LogEvent(logGroupName, logStreamName string) *LogEvent {
	logEvent := &LogEvent{
		Logs:          log,
		LogGroupName:  logGroupName,
		LogStreamName: logStreamName,
		Input: cloudwatchlogs.GetLogEventsInput{
			LogGroupName:  aws.String(logGroupName),
			LogStreamName: aws.String(logStreamName),
		},
	}

	return logEvent
}

func (le LogEvent) FindAll() ([]LogContent, error) {
	var events []LogContent

	for {
		items, err := le.Logs.client.GetLogEvents(&le.Input)
		if err != nil {
			return nil, err
		}

		for _, item := range items.Events {
			event := &LogContent{
				TimeStamp: aws.Int64Value(item.Timestamp),
				Message:   aws.StringValue(item.Message),
			}

			events = append(events, *event)
		}

		if aws.StringValue(items.NextBackwardToken) == aws.StringValue(le.Input.NextToken) {
			break
		}

		le.Input.NextToken = items.NextBackwardToken

	}

	return events, nil
}

func (le LogEvent) Save(message string) error {

	// トークンを取得する
	token, err := le.fetchToken()
	if err != nil {
		return err
	}

	// 時刻をミリ秒に変換する
	eventTime := now()

	var inputLogEvents []*cloudwatchlogs.InputLogEvent

	inputLogEvent := &cloudwatchlogs.InputLogEvent{
		Message:   aws.String(message),
		Timestamp: aws.Int64(eventTime),
	}

	inputLogEvents = append(inputLogEvents, inputLogEvent)
	return le.doSave(inputLogEvents, token)

}

func (le LogEvent) BatchSave(messages []string) error {

	// トークンを取得する
	token, err := le.fetchToken()
	if err != nil {
		return err
	}

	// 時刻をミリ秒に変換する
	eventTime := now()

	var inputLogEvents []*cloudwatchlogs.InputLogEvent

	for _, message := range messages {

		inputLogEvent := &cloudwatchlogs.InputLogEvent{
			Message:   aws.String(message),
			Timestamp: aws.Int64(eventTime),
		}

		inputLogEvents = append(inputLogEvents, inputLogEvent)
	}

	return le.doSave(inputLogEvents, token)

}

func (le LogEvent) fetchToken() (*string, error) {
	tokenInput := &cloudwatchlogs.DescribeLogStreamsInput{
		LogGroupName:        aws.String(le.LogGroupName),
		LogStreamNamePrefix: aws.String(le.LogStreamName),
	}

	out, err := le.Logs.client.DescribeLogStreams(tokenInput)
	if err != nil {
		return nil, err
	}

	if len(out.LogStreams) != 1 {
		return nil, errors.New("failed to identify the log stream")
	}

	return out.LogStreams[0].UploadSequenceToken, nil

}

func (le LogEvent) doSave(inputLogEvent []*cloudwatchlogs.InputLogEvent, token *string) error {
	input := &cloudwatchlogs.PutLogEventsInput{
		LogEvents:     inputLogEvent,
		LogGroupName:  aws.String(le.LogGroupName),
		LogStreamName: aws.String(le.LogStreamName),
		SequenceToken: token,
	}

	_, err := le.Logs.client.PutLogEvents(input)
	if err != nil {
		return err
	}

	return nil

}
