package logs

import "time"

func now() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func UnixMillisecond(target time.Time) int64 {
	return target.UnixNano() / int64(time.Millisecond)
}

func ParseTime(target string) (time.Time, error) {
	parsed, err := time.Parse(time.RFC3339, target)
	if err != nil {
		return time.Time{}, err
	}

	return parsed, nil
}

func Jst(target string) string {

	format := "2006-01-02 15:04:05.000"
	utc, _ := time.Parse(format, target)
	jst := time.FixedZone("Asia/Tokyo", 9*60*60)
	return utc.In(jst).Format(format)

}
