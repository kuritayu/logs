package logs

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
)

type LogGroup struct {
	Logs
	Names []string
	Input cloudwatchlogs.DescribeLogGroupsInput
}

func (log Logs) LogGroup() *LogGroup {
	logGroup := &LogGroup{
		Logs:  log,
		Input: cloudwatchlogs.DescribeLogGroupsInput{},
	}

	return logGroup

}

func (lg LogGroup) FindAll() ([]string, error) {

	var logGroups []string
	for {
		items, err := lg.Logs.client.DescribeLogGroups(&lg.Input)
		if err != nil {
			return nil, err
		}

		for _, item := range items.LogGroups {
			logGroups = append(logGroups, aws.StringValue(item.LogGroupName))
		}

		if aws.StringValue(items.NextToken) == "" {
			break
		}

		lg.Input.NextToken = items.NextToken

	}
	return logGroups, nil
}

func (lg LogGroup) Save(logGroup string) error {

	input := &cloudwatchlogs.CreateLogGroupInput{
		LogGroupName: aws.String(logGroup),
	}

	_, err := lg.Logs.client.CreateLogGroup(input)

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case cloudwatchlogs.ErrCodeResourceAlreadyExistsException:
				return nil
			default:
				return err
			}
		}

	}

	return nil

}

func (lg LogGroup) GrepByMessage(logGroup string, keyword string, from string, to string) (string, error) {

	// 日付レンジのパース
	ParsedFrom, err := ParseTime(from)
	if err != nil {
		return "", err
	}

	ParsedTo, err := ParseTime(to)
	if err != nil {
		return "", err
	}

	// クエリの構築
	query := fmt.Sprintf("fields @timestamp, @message, @logStream | filter @message like /%v/", keyword)

	// クエリ投入定義構築
	input := &cloudwatchlogs.StartQueryInput{
		EndTime:      aws.Int64(UnixMillisecond(ParsedTo)),
		LogGroupName: aws.String(logGroup),
		QueryString:  aws.String(query),
		StartTime:    aws.Int64(UnixMillisecond(ParsedFrom)),
	}

	// クエリ実行
	out, err := lg.Logs.client.StartQuery(input)
	if err != nil {
		return "", err
	}

	return aws.StringValue(out.QueryId), nil

}

type QueryResult struct {
	Timestamp string
	LogStream string
	Message   string
}

func (lg LogGroup) Result(query string) ([]QueryResult, error) {

	input := &cloudwatchlogs.GetQueryResultsInput{QueryId: aws.String(query)}

	out, err := lg.Logs.client.GetQueryResults(input)
	if err != nil {
		return nil, err
	}

	var result []QueryResult
	for _, record := range out.Results {

		var q QueryResult
		for _, element := range record {
			switch aws.StringValue(element.Field) {
			case "@timestamp":
				q.Timestamp = aws.StringValue(element.Value)
			case "@logStream":
				q.LogStream = aws.StringValue(element.Value)
			case "@message":
				q.Message = aws.StringValue(element.Value)
			default:
				continue
			}

		}
		result = append(result, q)

	}

	return result, nil

}
