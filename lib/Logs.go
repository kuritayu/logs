package logs

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
)

type Logs struct {
	client *cloudwatchlogs.CloudWatchLogs
}

func New(session *session.Session) *Logs {
	return &Logs{client: cloudwatchlogs.New(session)}
}
